''' Download and clean GKLP data '''

import pandas as pd
import numpy as np
import datetime
from bs4 import BeautifulSoup, SoupStrainer
import requests
import re
from gkcs import config
from gkcs import csv_data
from gkcs import subscriptions

gklp_path = 'http://learn-enclave.globalknowledge.com/csv/'
gklp_filepattern = 'gklp_enrollments.*.csv'
__gklp = pd.DataFrame()

# OLD
def download():
    
    filepattern = re.compile(gklp_filepattern)
    
    response = requests.get(gklp_path).text
    
    for link in BeautifulSoup(response, 'html.parser', parse_only=SoupStrainer('a')):
        if link.has_attr('href'):
            if filepattern.match(link['href']):
                return pd.read_csv(gklp_path + link['href'], escapechar='\\')
    return None


# get from site if not present in input_csvs directory (which would have been done manually)
def load(only_active=False):
    global __gklp

    local_filename_base = 'GKLP_raw_'

    # load csv or zip file
    gklp_raw = csv_data.load(local_filename_base)

    # if no local file, download
    if gklp_raw is None:
        print('Downloading')
        gklp_raw = download()
        if gklp_raw is not None:
            filename = local_filename_base + str(datetime.date.today())      
            csv_data.save_to_dir(gklp_raw, directory=config.INPUT_DIR, filename=filename, include_index=True, zip_file=False)
        else: # can't download either
            print('Cannot download GKLP data')
            return None

    if only_active:
        gklp_raw = gklp_raw[gklp_raw['Enrollment Active'] == 'active']

    # get only the results for our subscriptions
    sub_enroll_by_id = subscriptions.get_by_code()
    gklp_raw = gklp_raw[gklp_raw['Bundle ID'].isin(sub_enroll_by_id.index)]

    # "N" means not accessed at all = we replace that with null
    gklp_raw.loc[(gklp_raw['Course Progress'] == 'N'), 'Course Progress'] = None

    # "Course Progress" should only be an integer, which means course has been accessed at least
    gklp_accessed = gklp_raw[gklp_raw['Course Progress'].notnull()]
    
    # cast progress data to int so can be summed
    gklp_accessed = gklp_accessed.astype({'Course Progress': 'int64'})

    # Fix columns
    gklp_renamed = gklp_accessed[[
        'Email',
        'Enrollment ID',
        'Course ID',
        'Course Title',
        'Course Progress',
        'Course First Visit',
        'Course Last Visit',
        'Certificate',
    ]].copy()

    gklp_renamed.columns = [
        config.EMAIL,
        config.SUB_ENROLL_ID,
        config.CRS_CODE,
        config.CRS_TITLE,
        config.CRS_COMPLETE,
        config.CRS_START_DATE,
        config.CRS_LAST_ACCESS_DATE,
        config.CRS_CERT,
    ]

    gklp_renamed[config.CRS_MOD] = config.ON_DEMAND_GKLP

    # gklp_renamed[config.CRS_LAST_ACCESS_DATE] = pd.to_datetime(
    #     gklp_renamed[config.CRS_LAST_ACCESS_DATE],
    #     format=config.DATE_FORMAT, errors='coerce')

    # add enrollment status based on completion
    gklp_renamed[config.CRS_STATUS] = np.nan
    # complete does not require 100%
    gklp_in_progress = (gklp_renamed[config.CRS_COMPLETE] < config.GKLP_COMPLETE_CUTOFF)
    gklp_renamed.loc[gklp_in_progress, config.CRS_STATUS] = gklp_renamed.loc[gklp_in_progress, config.CRS_COMPLETE].apply(lambda p : str(round(p))+'%')
    gklp_renamed.loc[~gklp_in_progress, config.CRS_STATUS] = 'Done'

    __gklp = gklp_renamed

    return __gklp


# OLD
def get_by_sub_code(sub_code):
    gklp_sub = __gklp[__gklp['Bundle ID'] == sub_code]
    gklp_sub = gklp_sub.pivot_table(values='Course Progress',index='Enrollment ID',aggfunc=np.sum)
    return gklp_sub

