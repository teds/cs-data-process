''' Students '''

from gkcs import config
from gkcs import bookings

__students = None

def set_students(sub_bookings):
    global __students
    __students = sub_bookings

# Get dataframe indexed by email
def get_by_email_index():
    students_by_email = __students[[
        config.SUB_ENROLL_ID,
        config.EMAIL,
        config.FIRST_NAME,
        config.LAST_NAME,
    ]].copy()

    # drop nulls and duplicates (all fields considered, so may be duplicate indices)
    students_by_email.dropna(subset=[config.EMAIL], inplace=True)
    students_by_email.drop_duplicates(inplace=True)
    students_by_email.set_index(config.EMAIL, inplace=True)

    return students_by_email

# Get dataframe indexed by enrollment ID
def get_by_id_index():
    students_by_id = __students[[
        config.SUB_ENROLL_ID,
        config.EMAIL,
        config.FIRST_NAME,
        config.LAST_NAME,
    ]].copy()

    students_by_id.dropna(subset=[config.SUB_ENROLL_ID], inplace=True)
    students_by_id.drop_duplicates(inplace=True)
    students_by_id.set_index(config.SUB_ENROLL_ID, inplace=True)

    return students_by_id
