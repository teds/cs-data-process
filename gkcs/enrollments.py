''' Subscription Enrollments '''

import pandas as pd
from gkcs import bookings
from gkcs import config

__enrollments = pd.DataFrame()

# 
def set_enrolls(sub_bookings):
    global __enrollments
    __enrollments = sub_bookings
    return __enrollments

def get_enrolls():
    return __enrollments


# Get dataframe for specified subscription code
def get_by_sub_code(sub_code):
    return __enrollments[__enrollments[config.SUB_CODE] == sub_code].copy()


# Get dataframe indexed by email
def get_by_email_index():
    sub_enroll = __enrollments[[
        config.EMAIL,
        config.SUB_CODE,
        config.SUB_ENROLL_ID,
    ]].copy()

    sub_enroll.set_index([config.EMAIL], inplace=True)

    return sub_enroll

# Get dataframe indexed by both email and subscription code
def get_by_email_and_code():
    sub_enroll = __enrollments[[
        config.EMAIL,
        config.SUB_CODE,
        config.SUB_ENROLL_ID,
    ]].copy()

    sub_enroll.set_index([config.EMAIL, config.SUB_CODE], inplace=True)

    return sub_enroll

# Get dataframe indexed by enrollment ID
def get_by_id_index():
    sub_enroll_by_id = __enrollments[[
        config.EMAIL,
        config.FIRST_NAME,
        config.LAST_NAME,
        config.SUB_CODE,
        config.SUB_ENROLL_ID,
        config.SUB_START_DATE,
        config.SUB_AGE,
        config.SUB_BOOK_AMOUNT,
        config.SUB_PMT_METHOD,
    ]].copy()

    sub_enroll_by_id.set_index(config.SUB_ENROLL_ID, inplace=True)

    return sub_enroll_by_id
