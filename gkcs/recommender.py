''' Recommender '''

import pandas as pd
import numpy as np

from cpt.cpt import *
import pickle
from gkcs import config, csv_data

__course_list = None

__pickle_filename = 'cpt_2_model.p'

def save_course_list(master_data, report_end_date):
    global __course_list

    course_list = master_data[[
        config.CRS_CODE,
        config.CRS_TITLE,
        config.CRS_MOD,
    ]].copy()

    course_list.drop_duplicates(inplace=True)
    course_list.set_index(config.CRS_CODE, inplace=True)
    course_list.dropna(axis=0, inplace=True)

    __course_list = course_list

    csv_data.save_to_dir(__course_list, directory='/master/', filename='course_list_'+str(report_end_date), include_index=True)

    return __course_list

def load_course_list():
    course_list = csv_data.load('course_list_', directory='/master/')
    course_list = course_list.set_index(config.CRS_CODE)

    __course_list = course_list

    return __course_list

def save_model(model):
    pickle.dump(model, open(__pickle_filename, 'wb'))

def load_model():
    model = pickle.load(open(__pickle_filename, 'rb'))
    return model

def get_sequences(master_data):
    seq_array = []

    # get list of courses for each student
    courses_by_email = master_data[[
        config.EMAIL,
        config.CRS_CODE,
        config.CRS_START_DATE,
    ]].copy()
    # sort courses by student and start date
    seq_data = courses_by_email.sort_values([config.EMAIL, config.CRS_START_DATE])

    # clean
    seq_data = seq_data.drop(config.CRS_START_DATE, axis=1)
    seq_data = seq_data.dropna(axis=0)

    # index by student ID (email)
    seq_data = seq_data.set_index(config.EMAIL)

    # loop through each student and collect course sequences
    indices = seq_data.index.unique()
    for each_index in indices:
        seq = seq_data.loc[each_index].T.to_numpy()[0]
        seq_array.append(seq)

    return seq_array

# given master data, train model on course sequences per student
def train_model(master_data):

    model = CPT()
    model.train(get_sequences(master_data))

    return model

def get_courses_taken(report_list):
    courses_taken = report_list[[
        config.CRS_CODE,
        config.CRS_START_DATE,
    ]].copy()

    # order by when taken
    courses_taken = courses_taken.sort_values([config.CRS_START_DATE])
    courses_taken = courses_taken.drop(config.CRS_START_DATE, axis=1)

    return courses_taken

# get course recs from similar sequences
# last_x is number of last elements to use
# x_to_predict is number of predictions required
def get_course_recs(master_data, report_list, last_x=3, x_to_predict=1):

    array_len = len(get_sequences(master_data))

    # list of all courses (really just the courses that anyone has taken)
    course_list = load_course_list()

    courses_taken = get_courses_taken(report_list)

    # convert to consumable array
    student_crs_seq = list(courses_taken.T.to_numpy()[0])

    # get trained model to make recommendations
    model = load_model()
    # use model to get 
    predictions = model.predict(array_len, [student_crs_seq], last_x, x_to_predict)

    # recommended courses uses course numbers to look up modality and course title
    recommended_courses = course_list.loc[predictions[0], [config.CRS_MOD, config.CRS_TITLE]]

    return recommended_courses

# cert recommendations are based on courses taken
# All Access only for now
def get_cert_recs(single_student_activity, top_x=3):
    cert_list = []

    # first get course to cert correlations
    aa_cert_course = csv_data.load('cert_to_course', directory='/data/')
    aa_cert_course.columns = [
        config.CRS_TITLE,
        config.CRS_CODE,
        'Cert Code',
        'Cert Name'
    ]

    aa_cert_course = aa_cert_course.astype({config.CRS_CODE: 'str'})

    # get list of courses student took
    courses_taken = get_courses_taken(single_student_activity)
    unique_courses = courses_taken[config.CRS_CODE].str.replace('[A-Z]+$', '').unique()
    # Check each course to see if it matches with certificate
    for course in unique_courses:
        if course is np.nan:
            break
        crs_rows = aa_cert_course[aa_cert_course[config.CRS_CODE].str.contains(course)]
        for row in crs_rows['Cert Name']:
            cert_list.append(row)

    # for each cert name, order by how many times it appears (more means higher priority)
    certs_wtd = pd.Series(cert_list).value_counts().index
    # convert back to Series, take just the top few specified, back to dataframe
    recommended_certs = pd.Series(certs_wtd)[0:top_x].to_frame()

    return recommended_certs