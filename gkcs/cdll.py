""" load CDLL CSV and prepare data """

import pandas as pd
import numpy as np
from gkcs import config
from gkcs import csv_data
from gkcs import enrollments

# Downloaded from emailed link
cdll_filename_prefix = "export"
subscriber_id = "uid"

__cdll = pd.DataFrame()

def get_activity():
    activity = __cdll.pivot_table(values="medal_percent",index=subscriber_id,aggfunc=np.sum)
    activity = activity.rename_axis(config.EMAIL)
    return activity

# load and prepare dataframe
def load():
    global __cdll

    cdll = csv_data.load(cdll_filename_prefix)


    #Fix columns
    cdll_renamed = cdll[[
        'uid',
        'first_name',
        'last_name',
        'medal_percent',
        'course_uid', 
        'course_name',
        'first_launch',
    ]].copy()

    cdll_renamed.columns=[
        config.EMAIL,
        config.FIRST_NAME,
        config.LAST_NAME,
        config.CRS_COMPLETE,
        config.CRS_CODE,
        config.CRS_TITLE,
        config.CRS_START_DATE,
    ]

    # add subscription code
    # cdll_renamed[config.SUB_CODE] = config.CDLL['code'] NO! This would not include Cisco Elite!
    # need to get correct subscription

    # add subscription enrollment data
    enrolls_by_email = enrollments.get_by_email_index()
    # TODO:select only enrollments with the right date
    # merge
    cdll_renamed = cdll_renamed.merge(enrolls_by_email, how='inner', left_on=[config.EMAIL], right_index=True)

    # add course modality
    cdll_renamed[config.CRS_MOD] = config.ON_DEMAND_CISCO

    # add enrollment status based on completion
    cdll_renamed[config.CRS_STATUS] = np.nan
    cdll_in_progress = (cdll_renamed[config.CRS_COMPLETE] < 100)
    cdll_renamed.loc[cdll_in_progress, config.CRS_STATUS] = cdll_renamed.loc[cdll_in_progress,config.CRS_COMPLETE].apply(lambda p : str(round(p))+'%')
    cdll_renamed.loc[~cdll_in_progress, config.CRS_STATUS] = 'Done'

    __cdll = cdll_renamed

    return __cdll