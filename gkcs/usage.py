''' consolidate usage activity for each subscription '''

import pandas as pd
import numpy as np
from gkcs import config, gklp, skillsoft, ilt, cdll

# takes subscription name defined by config
def add_gklp(sub):
    gklp_subscribers = subscribers.get_by_sub_name(sub['name'])
    # get aggregated activity
    activity = gklp.get_gklp_by_sub_code(sub['code'])
    # combine using LEFT JOIN and keep index
    usage = gklp_subscribers.reset_index().merge(activity, how='left', left_on=config.SUB_ENROLL_ID, right_on='Enrollment ID').set_index(config.EMAIL)
    # specify usage column
    usage.rename(columns={'Course Progress':'GKLP Usage'}, inplace=True)

    return usage['GKLP Usage']

# takes subscription name defined by config
def add_skillsoft(sub):
    ss_subscribers = subscribers.get_by_sub_name(sub['name'])
    # get aggregated activity
    activity = skillsoft.get_ss_by_sub_name(sub['name'])
    # combine using LEFT JOIN
    usage = ss_subscribers.reset_index().merge(activity, how='left', left_on=config.SUB_ENROLL_ID, right_on='Username').set_index(config.EMAIL)
    # specify usage column
    usage.rename(columns={'Completion_Status':'Skillsoft Usage'}, inplace=True)

    return usage['Skillsoft Usage']

def add_ilt(sub):
    activity = ilt.get_ilt_by_sub_name(sub['name'])

    usage = activity.copy()

    usage.rename(columns={config.CRS_STATUS:'ILT Usage'}, inplace=True)

    return usage['ILT Usage']


# aggregate usage and return dataframe by given index (e.g. email or id)
def get_data(index, master_df):

    master = master_df.copy()
 
    # sum up usage for each enrollment and modality
    usage_pivot = master.pivot_table(values=config.CRS_COMPLETE, index=[index, config.CRS_MOD], aggfunc=np.sum)

    # create columns out of each modality
    usage_by_mod = usage_pivot.unstack(level=1, fill_value=0)
    usage_by_mod.columns = usage_by_mod.columns.droplevel(0)

    # merge usage with df
    student_usage = usage_by_mod.merge(master, how='right', on=index)
    
    # get latest last access date, but first convert to datetime (again!)
    student_usage[config.CRS_LAST_ACCESS_DATE] = pd.to_datetime(student_usage[config.CRS_LAST_ACCESS_DATE])
    last_access = student_usage.groupby(index)[[config.CRS_LAST_ACCESS_DATE]].max()
    
    student_usage.drop([
        config.CRS_CODE,
        config.CRS_COMPLETE,
        config.CRS_ENROLL_ID,
        config.CRS_EVENT_ID,
        config.CRS_TITLE,
        config.CRS_MOD,
        config.CRS_STATUS,
        config.CRS_START_DATE,
        config.CRS_CERT,
        config.CRS_LAST_ACCESS_DATE
        ], axis=1, inplace=True)
    
    student_usage.set_index(index)

    # unique only
    student_usage.drop_duplicates(subset=index, inplace=True)
    
    student_usage = student_usage.merge(last_access, how='inner', on=index)

    return student_usage

# this is not really necessary anymore
def get_per_email(master_df):
    return get_data(config.EMAIL, master_df)

# nor this
def get_per_id(master_df):
    return get_data(config.SUB_ENROLL_ID, master_df)