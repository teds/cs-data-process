''' create health report for each subscription '''

from gkcs import config, actions

def build(usage_df, end_date):

    def health_code(row):
        health = row[config.HEALTH_PTS]
        # categorize health points
        if health < config.HEALTH_CAUTION_LOW:
            return config.CRITICAL
        if (health >= config.HEALTH_CAUTION_LOW) and (health <= config.HEALTH_CAUTION_HIGH):
            return config.CAUTION
        return config.GOOD


    # only generate health for those subscriptions we have data for
    health_focus_mask = usage_df[config.SUB_CODE].isin(config.FOCUS_SUB_CODES)
    health_focus_df = usage_df[health_focus_mask].copy()

    # Replace NaNs with zeroes in modality usage columns, so that we can work with them.
    modality_cols = [
        config.ON_DEMAND_GKLP,
        config.ON_DEMAND_SKILLSOFT,
        config.ILT_CLASSROOM,
        config.ILT_VIRTUAL,
    ]
    # All modalities may not be in data
    for modality in modality_cols:
        if modality in health_focus_df.columns:
            health_focus_df[modality].fillna(value=0, inplace=True)
        else:
            health_focus_df[modality] = 0


    # get usage and multiply by weights
    gklp_weighted = health_focus_df[config.ON_DEMAND_GKLP] * config.WT_GKLP
    ss_weighted = health_focus_df[config.ON_DEMAND_SKILLSOFT] * config.WT_SKILLSOFT
    ilt_weighted = (health_focus_df[config.ILT_CLASSROOM] + health_focus_df[config.ILT_VIRTUAL]) * config.WT_ILT


    # sum of weighted activity
    sum_weighted = gklp_weighted + ss_weighted + ilt_weighted #+ cdll_weighted
    health_focus_df[config.HEALTH_PTS] = sum_weighted

    # assign descriptive category
    health_focus_df[config.HEALTH_SUMMARY] = health_focus_df.apply(health_code, axis=1)

    # add percentile for each subscription, to be combined with health
    rank_grouped = health_focus_df.groupby(by=config.SUB_CODE, axis=0, sort=True)[config.HEALTH_PTS].rank(method='min', pct=True)

    health_focus_df[config.CENTILE] = round(rank_grouped * 100).astype('int32')

    # actions for welcome, nudge, renew
    health_focus_df = actions.add(health_focus_df, end_date)

    return health_focus_df