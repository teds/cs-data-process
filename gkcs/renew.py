''' Renewal prompts to customers  '''
from gkcs import reports

def build_renew_email(single_student_activity):

    report = []

    fonts = '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">'
    css = '<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">'

    # filter for courses to be marked as complete
    completions = single_student_activity[config.CRS_COMPLETE]
    cert_filter = single_student_activity[config.CRS_CERT].str.startswith('http', na=False)

    # get list of complete courses
    complete = reports.get_complete(single_student_activity)

    # get list of in-progress courses
    in_progress = reports.get_in_progress(single_student_activity)

    student_data = single_student_activity.iloc[0,:]

    # demographic information
    email = student_data[config.EMAIL]
    name = student_data[config.FIRST_NAME]+' '+student_data[config.LAST_NAME]

    sub = student_data[config.SUB_NAME]

    # conditional sections
    def build_section(data, col_array):
        if len(data) > 0:
            data.columns = col_array
        else:
            data = None
        return data
    
    complete_section = build_section(complete, ['Title', 'Start Date', 'Format', 'Completion Certificate', 'Points Earned'])
    in_progress_section = build_section(in_progress, ['Title', 'Start Date', 'Format', 'Progress', 'Points Earned'])

    # Headers
    sub_header = 'Global Knowledge '+ sub
    prog_header = 'Achievements'
    complete_header = 'Completed'
    in_progress_header = 'In Progress'


    def tag_wrap(text, tag):
        return '<'+tag+'>'+text+'</'+tag+'>'

    # head
    # report.append('<!doctype html><html lang="en"><head>'+fonts+css+'</head>')

    # body
    report.append('<body><div class="container mt-5">')
    report.append(tag_wrap(sub_header, 'h1'))
    report.append(tag_wrap(prog_header, 'h2'))
    report.append(tag_wrap(weeks_in+' weeks into subscription, '+to_go+' weeks remaining', 'p'))

    # activity
    if complete_section is not None:
        report.append(tag_wrap(complete_header, 'h2'))
        report.append(complete_section.to_html(
            justify='center',
            na_rep='',
            index=False,
            border=0,
            classes=['table-striped']
            )
        )
    else:
        report.append(tag_wrap('No courses completed yet', 'h2'))
    report.append('<hr>')
    
    if in_progress_section is not None:
        report.append(tag_wrap(in_progress_header, 'h2'))
        report.append(in_progress.to_html(justify='center', na_rep='', index=False, border=0, classes=['table-striped']))
    else:
        report.append(tag_wrap('No courses in progress to report', 'h2'))

    report.append('</div></body></html>')

    html_report = ('').join(report)

    return renew_html, sub, student_data

def get_student_renews(data, enroll_id_arr):
    all_emails = []

    mail_from_address = 'customersuccess@globalknowledge.com'

    # we are only concerned with current students
    all_current_activity = get_all_current_activity(data)

    # report list is all activity for this student
    for enroll_id in enroll_id_arr:
        single_student_activity = all_current_activity[all_current_activity.iloc[:,5] == enroll_id]
        single_student_activity = single_student_activity.set_index(config.SUB_ENROLL_ID)

        # create report in HTML format
        html, sub_name, student_info = build_renew_email(single_student_activity)

        # for emails
        student_name = student_info[config.FIRST_NAME] +' '+ student_info[config.LAST_NAME]
        email = {
            'to': student_info[config.EMAIL],
            'subject': 'Time to renew your '+sub_name+' subscription',
            'content': html
        }
        all_emails.append(email)


    return all_emails