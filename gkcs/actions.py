''' generate lists to be acted upon '''

import pandas as pd
import numpy as np
from gkcs import config

 # Add columns depending on criteria
def add(df, end_date):
    subscription_length = pd.Timedelta(config.SUBSCRIPTION_WEEKS, unit='W')
    welcome_phase = pd.Timedelta(config.WELCOME_WEEKS, unit='W')
    onboard_phase = pd.Timedelta(config.ONBOARD_WEEKS, unit='W')
    use_phase = subscription_length - pd.Timedelta(config.RENEWAL_LAST_WEEKS, unit='W')

    activity_window = pd.Timedelta(config.ACTIVITY_WINDOW, unit='W')

    age = pd.to_timedelta(df[config.SUB_AGE], unit='W')

    # WELCOME
    df[config.WELCOME] = (age <= welcome_phase)

    # NUDGE
    # base nudges on no recent activity
    end_date = pd.to_datetime(end_date)
    last_access_dates = pd.to_datetime(df[config.CRS_LAST_ACCESS_DATE])
    weeks_since_last_access = (end_date - last_access_dates).apply(lambda x: round(x/np.timedelta64(1,'W')))

    df[config.NUDGE] = (
        (age > onboard_phase) & (age < use_phase) & (weeks_since_last_access > config.ACTIVITY_WINDOW)
        ).where(df[config.SUB_CODE].isin(config.FOCUS_SUB_CODES), other=None)

    # RENEW
    def get_renew(col_name):
        return (age + pd.Timedelta(col_name, unit='W') == subscription_length)

    df[config.RENEW_1] = get_renew(config.RENEWAL_1_WEEKS)
    df[config.RENEW_2] = get_renew(config.RENEWAL_2_WEEKS)
    df[config.RENEW_LAST] = get_renew(config.RENEWAL_LAST_WEEKS)
    df[config.RENEW_BY] = pd.to_datetime(df[config.SUB_START_DATE]) + pd.Timedelta(days=365)
    
    # SURVEY
    '''
    Future state criteria
    Onboarding survey (first occurence of any)
    - First successful enrollment in VILT
    - 20% completion of on-demand content
    - 1 month into subscription
    Learning survey (first occurence of any)
    - ompletion of two VILT courses
    - 150% completion of on-demand content
    - 6 months into subscription
    Realizing Value survey
    - 10 months into subscription
    '''

    # for now, just time
    df[config.SURVEY_ONBOARD] = (age + pd.Timedelta(4, unit='W') == subscription_length)
    df[config.SURVEY_LEARN] = (age + pd.Timedelta(26, unit='W') == subscription_length)
    df[config.SURVEY_REALIZE] = (age + pd.Timedelta(42, unit='W') == subscription_length)

    return df

# action specifies list
def generate_list(df, action, cols=[config.SUB_NAME, config.FIRST_NAME, config.LAST_NAME, config.EMAIL]):
    # select only rows that indicate this action is needed (they will be True or False)
    df_action = df[df[action]]
    action_list = df_action[cols].copy()
    action_list.sort_values(by=config.SUB_NAME, inplace=True)
    return action_list

def generate_survey_list():
    return