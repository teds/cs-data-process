''' Subscriptions '''

from gkcs import config
from gkcs import bookings

__subscriptions = None

mapping_dict = {
    ' It ': ' IT ',
    'Cdll': 'CDLL',
    '?': '-',
    'Ls': 'LS',
    'Asp.Net': 'ASP.NET',
    'Gk': 'GK'
}

# pass in subscription bookings data from report
def set_subs(sub_bookings):
    global __subscriptions

    def replace_all(series, mapping):
        for key in mapping:
            series = series.str.replace(key, mapping[key])
        return series

    subscriptions = sub_bookings.drop_duplicates(subset=[config.SUB_TAG]).copy()

    # work with only these columns
    subscriptions = subscriptions[[
        config.SUB_TAG, # index, temporary
        config.SUB_CODE, # index
        config.SUB_NAME,
        config.SUB_LIST_PRICE,
    ]]

    # names are in all CAPS, so fix that
    subscriptions[config.SUB_NAME] = subscriptions[config.SUB_NAME].str.title()
    subscriptions[config.SUB_NAME] = replace_all(subscriptions[config.SUB_NAME], mapping_dict)

    __subscriptions = subscriptions

# Get dataframe indexed by email
def get_by_tag():
    subscriptions_by_tag = __subscriptions[[
        config.SUB_TAG,
        config.SUB_CODE,
    ]]

    subscriptions_by_tag = subscriptions_by_tag.set_index(config.SUB_TAG)

    return subscriptions_by_tag

# Get dataframe indexed by subscription code
def get_by_code():
    subscriptions_by_code = __subscriptions[[
        config.SUB_CODE,
        config.SUB_NAME,
        config.SUB_LIST_PRICE,
    ]]

    subscriptions_by_code = subscriptions_by_code.set_index(config.SUB_CODE)

    return subscriptions_by_code
