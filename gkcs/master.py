''' Create master database of all subscriptions '''

import pandas as pd
import numpy as np

from gkcs import config, csv_data, bookings, enrollments, students
from gkcs import accounts, subscriptions, ilt, gklp, cdll, skillsoft, topics


__master = pd.DataFrame()

def initialize(to_date):
    # load
    sub_bookings, ilt_bookings = bookings.load(to_date)
    topics.load()

    # initialize
    enrollments.set_enrolls(sub_bookings)
    students.set_students(sub_bookings)
    accounts.set_accounts(sub_bookings)
    subscriptions.set_subs(sub_bookings)
    ilt.set_ilt(ilt_bookings)

def get_all_activity():

    # load data to use
    ilt_data = ilt.get_ilt()
    gklp_data = gklp.load()
    # cdll_data = cdll.load()
    skillsoft_data = skillsoft.load()

    # combine data into one dataframe
    master_df = pd.concat([
        ilt_data,
        gklp_data,
        # cdll_data,
        skillsoft_data
    ], axis=0, join='outer', sort=True)

    return master_df

def add_no_activity(master_df):

    # add subscription enrollment
    master_df = master_df.merge(enrollments.get_by_id_index(), how='right', left_on=config.SUB_ENROLL_ID, right_index=True, indicator='merge_from')

    # drop old subscription code and use new one
    master_df.drop('Subscription Code_x', axis=1, inplace=True)
    master_df.rename(columns={'Subscription Code_y': config.SUB_CODE}, inplace = True)

    # Add emails
    no_activity = master_df['merge_from'] == 'right_only'
    master_df.loc[no_activity, 'Student Email_x'] = master_df.loc[no_activity, 'Student Email_y']
    master_df.drop(['Student Email_y', 'merge_from'], axis=1, inplace=True)
    master_df.rename(columns={'Student Email_x': config.EMAIL}, inplace = True)

    return master_df

def add_topics(master_df):
    topics_df = topics.get_topics()

    master_df = master_df.merge(topics_df, how='left', left_on=config.CRS_CODE, right_index=True)

    return master_df



def clean_up(master_df):

    # Replace NaN with values where appropriate
    def fill_missing_values(master_df):
        # add missing values
        def replace_nan(df, to_replace, key, replace_from_df):
            nulls = df[to_replace].isnull()
            # nothing needs to be done if no nulls
            if nulls.sum() > 0:
                df.loc[nulls, to_replace] = df.loc[nulls, key].map(replace_from_df[to_replace])
            return df

        # use subscription enrollment ID to add first, last names
        students_by_sub_enroll_id = students.get_by_id_index()
        master_df = replace_nan(master_df, config.FIRST_NAME, config.SUB_ENROLL_ID, students_by_sub_enroll_id)
        master_df = replace_nan(master_df, config.LAST_NAME, config.SUB_ENROLL_ID, students_by_sub_enroll_id)

        # If we still have missing first/last names, use email
        students_by_email = students.get_by_email_index()
        master_df = replace_nan(master_df, config.FIRST_NAME, config.EMAIL, students_by_email)
        master_df = replace_nan(master_df, config.LAST_NAME, config.EMAIL, students_by_email)

        # add subscription code if missing
        sub_enroll_by_id = enrollments.get_by_id_index()
        master_df = replace_nan(master_df, config.SUB_CODE, config.SUB_ENROLL_ID, sub_enroll_by_id)

        # add account information
        accounts_by_sub_enroll_id = accounts.get_by_id_index()
        master_df = master_df.merge(accounts_by_sub_enroll_id, how='left', left_on=config.SUB_ENROLL_ID, right_index=True)

        # add subscription information
        subscriptions_by_code = subscriptions.get_by_code()
        master_df = master_df.merge(subscriptions_by_code, how='left', left_on=config.SUB_CODE, right_index=True)

        return master_df

    def fix_dates(master_df):

        def conform_date_format(date_series):
            return pd.to_datetime(date_series, errors='coerce').dt.date

        master_df[config.SUB_START_DATE] = conform_date_format(master_df[config.SUB_START_DATE])
        master_df[config.CRS_START_DATE] = conform_date_format(master_df[config.CRS_START_DATE])
        master_df[config.CRS_BOOK_DATE] = conform_date_format(master_df[config.CRS_BOOK_DATE])

        # ILT Last Access - Attended
        att_filter = master_df[config.CRS_STATUS] == "Attended"
        ilt_end_dates = master_df[config.CRS_START_DATE] + pd.Timedelta(config.ILT_LENGTH_DAYS, unit='D')
        master_df.loc[att_filter, config.CRS_LAST_ACCESS_DATE] = ilt_end_dates
        # ILT Last Access - Confirmed
        con_filter = master_df[config.CRS_STATUS] == "Confirmed"
        master_df.loc[con_filter, config.CRS_LAST_ACCESS_DATE] = master_df[config.CRS_BOOK_DATE]

        # No access ever
        nan_filter = master_df[config.CRS_LAST_ACCESS_DATE].isnull()
        master_df.loc[nan_filter, config.CRS_LAST_ACCESS_DATE] = master_df[config.SUB_START_DATE]

        return master_df

    def fix_course_codes(master_df):
        master_df[config.CRS_CODE] =  master_df[config.CRS_CODE].str.replace('[CLAW].*', '', regex=True)

        return master_df

    master_df = fill_missing_values(master_df)

    # remove rows with blank emails
    master_df = master_df[master_df[config.EMAIL].notnull()]

    # remove global knowledge employees
    master_df = master_df[~master_df[config.EMAIL].str.contains('globalknowledge.com')]

    master_df = fix_dates(master_df)

    # fix course codes
    master_df[config.CRS_CODE] =  master_df[config.CRS_CODE].str.replace('[CLAW].*', '', regex=True)

    return master_df

# initialize and assemble
def build(to_date):
    
    initialize(to_date)

    # build master dataframe
    master_df = get_all_activity()
    master_df = add_no_activity(master_df)

    master_df = master_df.reset_index(drop=True)
    # get rid of the cruft - conform and consolidate
    master_df = clean_up(master_df)
    # now we can include topics
    master_df = add_topics(master_df)


    # reorder columns because why not
    master_df = master_df[[
        config.ACCOUNT,
        config.EMAIL,
        config.FIRST_NAME,
        config.LAST_NAME,
        config.SUB_CODE,
        config.SUB_NAME,
        config.SUB_LIST_PRICE,
        config.SUB_ENROLL_ID,
        config.SUB_START_DATE,
        config.SUB_AGE,
        config.SUB_BOOK_AMOUNT,
        config.SUB_PMT_METHOD,
        config.CRS_ENROLL_ID,
        config.CRS_EVENT_ID,
        config.CRS_CODE,
        config.CRS_TITLE,
        config.CRS_MOD,
        config.CRS_CAT,
        config.CRS_TYPE,
        config.CRS_STATUS,
        config.CRS_START_DATE,
        config.CRS_LAST_ACCESS_DATE,
        config.CRS_COMPLETE,
        config.CRS_CERT,
    ]]

    __master = master_df

    return __master

def get_master():
    return __master