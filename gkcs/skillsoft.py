""" load Skillsoft CSV and prepare data """

import pandas as pd
import numpy as np
from gkcs import config, csv_data, enrollments

# Downloaded from emailed link
ss_filename_prefix = "gk subscription activity"
ss_ultimate_code = 8426
ss_all_access_code = 8373

__skillsoft = pd.DataFrame()

#OLD
# get dataframe formatted correctly
def prepare(ss_df):
    # replace status strings with config values
    ss_df["Completion_Status"].replace(
        {"In Progress": config.SKILLSOFT_IN_PROGRESS, "Completed": config.SKILLSOFT_COMPLETED}, inplace=True)
    # conform usernames
    ss_df['Username'] = ss_df['Username'].str.upper()

    return ss_df

# OLD
def get_ss_by_sub_name(sub_name):
    ss_sub = None
    if sub_name == config.ULTIMATE['name']:
        ss_sub = __skillsoft[__skillsoft["Group_Org_Code"] == ss_ultimate_code]
        ss_sub = ss_sub.pivot_table(values="Completion_Status",index="Username",aggfunc=np.sum)
    elif sub_name == config.ALL_ACCESS['name']:
        ss_sub = __skillsoft[__skillsoft["Group_Org_Code"] == ss_all_access_code]
        ss_sub = ss_sub.pivot_table(values="Completion_Status",index="Username",aggfunc=np.sum)
    else:
        print(sub_name+" not found")
    return ss_sub

# load and prepare dataframe
def load():
    global __skillsoft

    ss = csv_data.load(ss_filename_prefix)

    # replace status strings with config values
    ss["Completion_Status"].replace(
        {"In Progress": config.SKILLSOFT_IN_PROGRESS, "Completed": config.SKILLSOFT_COMPLETED}, inplace=True)

    # conform usernames
    ss['Username'] = ss['Username'].str.upper()

    # Fix columns
    ss_renamed = ss[[
        'Username',
        'Group_Org_Code',
        'Asset_ID',
        'Asset_Title',
        'First_Access_Date',
        'Last_Access_Date',
        'Completion_Status',
    ]].copy()

    ss_renamed.columns=[
        config.SUB_ENROLL_ID,
        config.SUB_CODE,
        config.CRS_CODE,
        config.CRS_TITLE,
        config.CRS_START_DATE,
        config.CRS_LAST_ACCESS_DATE,
        config.CRS_COMPLETE,
    ]

    # Correct subscription codes
    ss_renamed[config.SUB_CODE] = ss_renamed[config.SUB_CODE].map({8426: '7950A', 8373: '8373A'})

    # add subscription enrollment data
    sub_enrolls = enrollments.get_by_id_index()
    sub_enrolls_emails = sub_enrolls[config.EMAIL].reset_index()
    ss_renamed = ss_renamed.merge(sub_enrolls_emails, how='inner', on=config.SUB_ENROLL_ID)

    # add course modality
    ss_renamed[config.CRS_MOD] = config.ON_DEMAND_SKILLSOFT

    # add enrollment status based on completion
    ss_renamed[config.CRS_STATUS] = ss_renamed[config.CRS_COMPLETE].replace({
        config.SKILLSOFT_COMPLETED: 'Completed', 
        config.SKILLSOFT_IN_PROGRESS: 'In Progress',
        })

    __skillsoft = ss_renamed

    return __skillsoft


    