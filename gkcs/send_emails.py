''' Configure and Send Email '''

import win32com.client as win32
from gkcs import config

outlook = win32.Dispatch('outlook.application')

'''
takes array of email objects of the form:
    {
        'to': email address,
        'subject': subject line,
        'content': HTML content
    }

'''
def send(email_arr, send=False):

    def email_action(email, send):  

        mail_item = outlook.CreateItem(0)   # 0: olMailItem

        mail_item = outlook.CreateItem(0)
        mail_item.To = email['to']
        mail_item.SendUsingAccount = config.EMAILS_FROM
        mail_item.SentOnBehalfOfName = config.EMAILS_FROM
        mail_item.Subject = email['subject']
        mail_item.HTMLBody = email['content']
        # mail_item.Attachments.Add('path/to/attachment') # To attach a file to the email (optional)
        # mail_item.Display(False) # to open in window
        # to send immediately
        if send:
            mail_item.Send()
        else: 
            mail_item.Save()

    for email in email_arr:
        email_action(email, send)