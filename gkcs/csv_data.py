""" load CSV file from local dir and return pandas dataframe and save data as CSV """

import glob
import pandas as pd
from gkcs import config, csv_data

# load from specified directory
def load(filename, directory=config.INPUT_DIR):
    # returns first one found
    filelist = glob.glob(config.WORKING_DIR + directory + filename + "*.*")
    try:
        df = pd.read_csv(filelist[0], escapechar="\\")
    except IndexError:
        print("No existing file found for "+filename)
        return None
    return df


# save to specified directory
def save_to_dir(df, directory='', filename='default_output.csv', include_index=False, zip_file=False):

    filepath = config.WORKING_DIR + directory + filename + '.csv'

    df.to_csv(filepath, index = include_index, header=True)

# save to default output directory
def save(df, directory='', filename='default_output.csv', include_index=False, zip_file=False):

    dir_path = config.WORKING_DIR + config.OUTPUT_DIR + directory

    save_to_dir(df, directory=dir_path, filename=filename, include_index=include_index, zip_file=zip_file)

