''' load bookings report and prepare subscriber dataframes '''

import pandas as pd
import numpy as np
from gkcs import config
from gkcs import csv_data

# 'Data' tab from Katie's Subscription Bookings Report
bookings_filename_prefix = 'Subscriptions Bookings Report'

__sub_bookings = None
__ilt_bookings = None

# format and split into subscriptions and ILT

# Subscription bookings need extra care
def prepare_sub_bookings(sub_bookings_df, end_date):
    # make copy instead of view
    sub_bookings = sub_bookings_df.copy()
    
    # Split CUST_NAME into first and last, proper case
    cust_names = sub_bookings["CUST_NAME"].str.title()
    sub_bookings[[config.FIRST_NAME, config.LAST_NAME]] = cust_names.str.split(' ', 1, expand=True)
    
    # unneeded columns
    sub_bookings.drop([
        'CUST_NAME',
        'EVENT_ID',
        'COURSE_MOD',
        'ENROLL_STATUS',
    ], axis=1, inplace=True)

    # New column names
    sub_bookings.rename(columns={
        'COURSE_CODE': config.SUB_CODE,
        'COURSE_NAME': config.SUB_NAME,
        'REVISED_START_DATE': config.SUB_START_DATE,
        'ENROLL_ID': config.SUB_ENROLL_ID,
    }, inplace=True)


    # calculate age of subscription
    sub_bookings[config.SUB_AGE] = (
           pd.to_datetime(end_date) - sub_bookings[config.SUB_START_DATE]).apply(
           lambda x: round(x/np.timedelta64(1,'W')))

    return sub_bookings

# ILT bookings need extra care too
def prepare_ilt_bookings(ilt_bookings_df):
    # make copy instead of view
    ilt_bookings = ilt_bookings_df.copy()

    # unneeded columns
    ilt_bookings.drop([
        'CUST_NAME',
        config.SUB_LIST_PRICE,
        config.SUB_BOOK_AMOUNT,
        config.SUB_PMT_METHOD,
        config.ACCOUNT,
    ], axis=1, inplace=True)

    # New column names
    ilt_bookings.rename(columns={
        'COURSE_CODE': config.CRS_CODE,
        'COURSE_NAME': config.CRS_TITLE,
        'ENROLL_ID': config.CRS_ENROLL_ID,
        'EVENT_ID': config.CRS_EVENT_ID,
        'REVISED_START_DATE': config.CRS_START_DATE,
        'COURSE_MOD': config.CRS_MOD,
        'ENROLL_STATUS': config.CRS_STATUS,
    }, inplace=True)

    return ilt_bookings

# Process bookings dataframe and extract ILT and subscription bookings
def prepare(bookings_df, end_date):

    bookings = bookings_df.copy()
    # remove unused columns
    bookings.drop([
        'KEYCODE',
        'CURR_CODE',
        'Fiscal Book Yr',
        'Fiscal Book Mo',
        'BOOK_AMT (LC)',
        'Fx Rate',
        'DO_NOT_USE_START_DATE',
    ], axis=1, inplace=True)

    # rename
    bookings.rename(columns={
        'ACCT_NAME': config.ACCOUNT,
        'EMAIL': config.EMAIL,
        'LIST_PRICE': config.SUB_LIST_PRICE,
        'BOOK_AMT (USD)': config.SUB_BOOK_AMOUNT,
        'BOOK_DATE': config.CRS_BOOK_DATE,
        'PAYMENT_METHOD': config.SUB_PMT_METHOD,
    }, inplace=True)

    # convert date strings to date format
    bookings['REVISED_START_DATE'] = pd.to_datetime(bookings['REVISED_START_DATE'])

    # Remove cancelled and did not attend
    bookings = bookings[((bookings['ENROLL_STATUS'] == 'Attended') | (bookings['ENROLL_STATUS'] == 'Confirmed'))]

    # split 
    bookings_ilt_filter = (
        (bookings[config.SUB_TAG] == config.ALL_ACCESS['ilt']) |
        (bookings[config.SUB_TAG] == config.DISCOVERY['ilt'])
        # |(bookings['Subscription Tag'] == config.CISCO_ELITE['ilt'])
        )

    ilt_bookings = bookings[bookings_ilt_filter]
    sub_bookings = bookings[~bookings_ilt_filter]

    sub_bookings = prepare_sub_bookings(sub_bookings, end_date)
    ilt_bookings = prepare_ilt_bookings(ilt_bookings)

    return sub_bookings, ilt_bookings

def purge(bookings_df):
    bookings = bookings_df.copy()
    roster_purge = csv_data.load('roster_purge', directory='/data/')
    
    # identify invalid listings
    bookings = bookings.merge(
        roster_purge,
        how='outer', 
        left_on=config.EMAIL,
        right_on='Email',
        indicator=True)


    # remove invalid listings (will have _merge value of 'both')
    removed = bookings[bookings['_merge'] == 'both']
    bookings = bookings[bookings['_merge'] == 'left_only']
    
    # clean up
    bookings = bookings.drop(['Email', '_merge'], axis=1)

    return bookings

def load(end_date, report=bookings_filename_prefix):
    global __sub_bookings
    global __ilt_bookings

    bookings_df = csv_data.load(report)
    if bookings_df is None:
        print("No Subscriptions Bookings Report found")
        return None, None

    sub_bookings, ilt_bookings = prepare(bookings_df, end_date)

    sub_bookings = purge(sub_bookings)

    __sub_bookings = sub_bookings
    __ilt_bookings = ilt_bookings

    return __sub_bookings, __ilt_bookings

def get_sub_bookings():
    if __sub_bookings is None:
        print("Bookings not loaded!")
    return __sub_bookings

def get_ilt_bookings():
    if __ilt_bookings is None:
        print("Bookings not loaded!")
    return __ilt_bookings

def get_by_sub_name(sub_tag):
    this_sub_bookings = __sub_bookings[__sub_bookings[config.SUB_TAG] == sub_tag]
    this_sub_bookings = this_sub_bookings.set_index(config.EMAIL)
    return this_sub_bookings

def get_by_ilt_name(ilt_tag):
    this_ilt_bookings = __ilt_bookings[__ilt_bookings[config.SUB_TAG] == ilt_tag]
    this_ilt_bookings = this_ilt_bookings.set_index(config.EMAIL)
    return this_ilt_bookings