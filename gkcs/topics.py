''' Courses '''

from gkcs import config, csv_data

__topics = None

# 
def get_topics():

    return __topics

# load and prepare dataframe
def load():
    global __topics

    topics = csv_data.load('course-cat-type', directory='/data/')

    topics.columns = [
        config.CRS_CODE,
        config.CRS_CAT,
        config.CRS_TYPE
    ]

    topics[config.CRS_CODE] = topics[config.CRS_CODE].apply(str)

    topics.set_index(config.CRS_CODE, inplace=True)

    topics.dropna(axis=0, how='all', inplace=True)

    __topics = topics

    return __topics
