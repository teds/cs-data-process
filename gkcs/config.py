'''
               CONFIGURATION FILE
         with frequently-used constants
'''

DATE_FORMAT = '%m/%d/%Y'

'''
***************** TIMELINE *****************
'''
SUBSCRIPTION_WEEKS = 52
WELCOME_WEEKS = 1
ONBOARD_WEEKS = 4
ILT_LENGTH_DAYS = 7
ACTIVITY_WINDOW = 4
COHORT_WINDOW = 4
RENEWAL_1_WEEKS = 12
RENEWAL_2_WEEKS = 4
RENEWAL_LAST_WEEKS = 1
'''
***************** ADMIN *****************
'''
EMAILS_FROM = 'CustomerSuccess@globalknowledge.com'

# directories
WORKING_DIR = '.'
OUTPUT_DIR = '/data/'
INPUT_DIR = '/data/'
'''
***************** SUBSCRIPTION TEXT *****************
'''
# fields (columns)
ACCOUNT = 'Account Name'
EMAIL = 'Student Email'
FIRST_NAME = 'Student First Name'
LAST_NAME = 'Student Last Name'
SUB_TAG = 'Subscription Tag'
SUB_CODE = 'Subscription Code'
SUB_NAME = 'Subscription Name'
SUB_LIST_PRICE = 'Subscription List Price'
SUB_ENROLL_ID = 'Subscription Enrollment ID'
SUB_START_DATE = 'Subscription Start Date'
SUB_AGE = 'Subscription Age in Weeks'
SUB_BOOK_AMOUNT = 'Subscription Book Amount'
SUB_PMT_METHOD = 'Subscription Payment Method'
CRS_BOOK_DATE = 'Course Book Date'
CRS_ENROLL_ID = 'Course Enrollment ID'
CRS_EVENT_ID = 'Course Event ID'
CRS_CODE = 'Course Code'
CRS_TITLE = 'Course Title'
CRS_CAT = 'Course Category'
CRS_TYPE = 'Course Type'
CRS_MOD = 'Course Modality'
CRS_STATUS = 'Course Enrollment Status'
CRS_START_DATE = 'Course Start Date'
CRS_LAST_ACCESS_DATE = 'Last Access Date'
CRS_COMPLETE = 'Course Percent Complete'
CRS_CERT = 'Course Certificate'

COMM_TYPE = 'Communication Type'
COMM_DATE = 'Communication Date'

# source
GKLP = 'GKLP'
SKILLSOFT = 'Skillsoft'
ILT = 'ILT'
DEVELOP = 'Develop.com'
# CDLL = 'CDLL'

# modality
ON_DEMAND_GKLP = 'Enhanced On-Demand'
ON_DEMAND_SKILLSOFT = 'Standard On-Demand'
# ON_DEMAND_CISCO = 'Cisco Digital'
ILT_VIRTUAL = 'Virtual'
ILT_CLASSROOM = 'Classroom'

# list types
RENEW_1 = 'Renew 1'
RENEW_2 = 'Renew 2'
RENEW_LAST = 'Renew Last'
RENEWED = 'Renewed'
RENEW_BY = 'Renew by Date'
WELCOME = 'Welcome'
NUDGE = 'Nudge'
SURVEY_ONBOARD = 'NPS Survey: Onboard'
SURVEY_LEARN = 'NPS Survey: Learn'
SURVEY_REALIZE = 'NPS Survey: Realize'

# subscriptions that get reports
DISCOVERY = {
    'code': '100785A',
    'name': 'GK POLARIS: DISCOVERY',
    'ilt': 'POLARIS DISCOVERY ILT',
    'source': [ILT],
}

LAUNCH = {
    'code': '100783W',
    'name': 'GK POLARIS: LAUNCH',
    'source': [DEVELOP],
}

ALL_ACCESS = {
    'code': '8373A',
    'name': 'ALL ACCESS',
    'ilt': 'ALL ACCESS ILT',
    'source': [GKLP, SKILLSOFT, ILT],
}

ULTIMATE = {
    'code': '7950A',
    'name': 'ULTIMATE IT SKILLS',
    'source': [GKLP, SKILLSOFT],
}

# CISCO_ELITE = {
#     'code': '5102W',
#     'name': 'CISCO ELITE',
#     'ilt': 'CISCO ELITE ILT',
#     'source': [ILT, CDLL],
# }

# CDLL = {
#     'code': '6572W',
#     'name': 'CISCO CDLL',
#     'source': [CDLL],
# }

MTA = {
    'code': '3846A',
    'name': 'MS TOTAL ACCESS',
    'source': [GKLP],
}


FOCUS_SUB_CODES = [
    DISCOVERY['code'],
    LAUNCH['code'],
    ALL_ACCESS['code'],
    ULTIMATE['code'],
    # CISCO_ELITE['code'],
    # CDLL['code'],
    MTA['code'],
]

# Renewal identifiers
SR = 'Renewal by Subscriber'
SX = 'Cross-purchase by Subscriber'
AR = 'Renewal by Account'
AX = 'Cross-purchase by Account'


'''
***************** HEALTH-RELATED *****************
'''

# health fields
HEALTH_PTS = 'Health Points'
HEALTH_SUMMARY = 'Activity Summary'
CENTILE = 'Percentile Rank'

# points for status (used for health)
ILT_ATTENDED = 100
ILT_CONFIRMED = 50
SKILLSOFT_COMPLETED = 100
SKILLSOFT_IN_PROGRESS = 50
GKLP_COMPLETE_CUTOFF = 97 # percent completed for points (not a decimal!)

# weighting of source
WT_ILT  = 2.5
WT_GKLP = 1.0
# WT_CDLL = 1.0
WT_SKILLSOFT   = 0.2

# health score categories
HEALTH_CAUTION_LOW = 1 # lower than this is critical
HEALTH_CAUTION_HIGH = 200 # higher than this is good

# markers used in report
CRITICAL = 'Little to no activity'
CAUTION = 'Some activity, could be better'
GOOD  = 'Getting good value'
