''' Reports - Personalized reports '''

import random
import os
import os
import pandas as pd

# USING ReportLab https://bitbucket.org/rptlab/reportlab/src/default/
# GUIDE: https://medium.com/@vonkunesnewton/generating-pdfs-with-reportlab-ced3b04aedef
# GUIDE: https://www.reportlab.com/docs/reportlab-userguide.pdf
from reportlab.platypus import SimpleDocTemplate
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import Image
from reportlab.platypus import Paragraph
from reportlab.platypus import Spacer
from reportlab.platypus import Table

from gkcs import config, csv_data, recommender

# get list of completed courses only
def get_complete(activity):
    completions = activity[config.CRS_COMPLETE]
    # filters
    cert_filter = activity[config.CRS_CERT].str.startswith('http', na=False)
    complete_filter = (completions >= 100) | cert_filter
    # list
    complete_list = activity[complete_filter]
    complete = complete_list[[
        config.CRS_TITLE,
        # config.CRS_START_DATE,
        config.CRS_MOD,
        config.CRS_CERT,
        # config.HEALTH_PTS
    ]].copy()
    complete.fillna('',inplace=True)

    return complete

# get list of in-progress courses
def get_in_progress(activity):
    completions = activity[config.CRS_COMPLETE]
    # filters
    cert_filter = activity[config.CRS_CERT].str.startswith('http', na=False)
    complete_filter = (completions >= 100) | cert_filter
    in_progress_filter = (completions > 0) & ~complete_filter
    # list
    in_progress_list = activity[in_progress_filter]
    in_progress = in_progress_list[[
        config.CRS_TITLE,
        # config.CRS_START_DATE,
        config.CRS_MOD,
        config.CRS_STATUS,
        # config.HEALTH_PTS
    ]].copy()

    return in_progress

# compile HTML report
def build_report_html(focus_subs_data, single_student_activity):
    data = focus_subs_data.copy()

    report = []

    total_points = single_student_activity[config.HEALTH_PTS].sum()

    # get list of complete courses
    complete = get_complete(single_student_activity)

    # get list of in-progress courses
    in_progress = get_in_progress(single_student_activity)

    # get course recommendations and cert recommendations
    course_recs_list = recommender.get_course_recs(data, single_student_activity, x_to_predict=2)
    course_recs = course_recs_list[[
        config.CRS_TITLE,
        config.CRS_MOD,
    ]]

    cert_recs = recommender.get_cert_recs(single_student_activity, top_x=4)

    student_data = single_student_activity.iloc[0,:]

    # demographic information
    email = student_data[config.EMAIL]
    name = student_data[config.FIRST_NAME]+' '+student_data[config.LAST_NAME]
    age = int(student_data[config.SUB_AGE])
    weeks_in = str(age)
    to_go = str(52 - age)
    sub = student_data[config.SUB_NAME]

    
    fonts = '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">'
    css = '<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">'


    # conditional sections
    def build_section(data, col_array):
        if len(data) > 0:
            data.columns = col_array
        else:
            data = None
        return data
    
    complete_section = build_section(complete, ['Title', 'Start Date', 'Format', 'Completion Certificate', 'Points Earned'])
    in_progress_section = build_section(in_progress, ['Title', 'Start Date', 'Format', 'Progress', 'Points Earned'])
    course_recs_section = build_section(course_recs, ['Title', 'Format'])
    cert_recs_section = build_section(cert_recs, ['Certification'])

    # Headers
    sub_header = 'Global Knowledge '+ sub
    prog_header = 'Progress Report for '+ name
    complete_header = 'Completed'
    in_progress_header = 'Not Yet Completed'
    course_recs_header = 'Courses to Consider'
    cert_recs_header = 'Certifications to Consider'

    def tag_wrap(text, tag):
        return '<'+tag+'>'+text+'</'+tag+'>'

    # head
    # report.append('<!doctype html><html lang="en"><head>'+fonts+css+'</head>')

    # body
    report.append('<body><div class="container mt-5">')
    report.append(tag_wrap(sub_header, 'h1'))
    report.append(tag_wrap(prog_header, 'h2'))
    report.append(tag_wrap(weeks_in+' weeks into subscription, '+to_go+' weeks remaining', 'p'))
    report.append(tag_wrap(str(total_points)+' total points earned', 'p'))

    # activity
    if complete_section is not None:
        report.append(tag_wrap(complete_header, 'h2'))
        report.append(complete_section.to_html(
            justify='center',
            na_rep='',
            index=False,
            border=0,
            classes=['table-striped']
            )
        )
    else:
        report.append(tag_wrap('No courses completed yet', 'h2'))
    report.append('<hr>')
    
    if in_progress_section is not None:
        report.append(tag_wrap(in_progress_header, 'h2'))
        report.append(in_progress.to_html(justify='center', na_rep='', index=False, border=0, classes=['table-striped']))
    else:
        report.append(tag_wrap('No courses in progress to report', 'h2'))
    report.append('<hr>')
        
    # recommended courses
    if course_recs_section is not None:
        report.append(tag_wrap(course_recs_header, 'h2'))
        report.append(course_recs_section.to_html(justify='center', na_rep='', index=False, border=0, classes=['table-striped']))
        report.append('<hr>')

    # recommended certifications
    if cert_recs_section is not None:
        report.append(tag_wrap(cert_recs_header, 'h2'))
        report.append(cert_recs_section.to_html(justify='center', na_rep='', index=False, header=False, border=0, classes=['table-striped']))

    report.append('</div></body></html>')

    html_report = ('').join(report)

    return html_report

# all data from master, filtered for only current students
def get_all_current_activity(data):
    
    all_current_activity = data[
        (data[config.SUB_AGE] > 0) & (data[config.SUB_AGE] <= config.SUBSCRIPTION_WEEKS)].copy()

    # drop unneeded columns
    all_current_activity.drop([
        #config.ACCOUNT,
        config.SUB_LIST_PRICE,
        config.SUB_BOOK_AMOUNT,
        config.CRS_EVENT_ID,
    ], axis=1, inplace=True)

    # sort by enrollment ID, then by date
    all_current_activity.sort_values(by=[config.SUB_ENROLL_ID, config.CRS_START_DATE], axis=0, ascending=True, inplace=True)

    return all_current_activity

# returns all data from master in same format as get_all_current_activity
def get_all_activity(data):
    
    all_activity = data.copy()

    # drop unneeded columns
    all_activity.drop([
        #config.ACCOUNT,
        config.SUB_LIST_PRICE,
        config.SUB_BOOK_AMOUNT,
        config.CRS_EVENT_ID,
    ], axis=1, inplace=True)

    # sort by enrollment ID, then by date
    all_activity.sort_values(by=[config.SUB_ENROLL_ID, config.CRS_START_DATE], axis=0, ascending=True, inplace=True)

    return all_activity

def get_all_report_emails(data, enroll_id_arr):
    all_emails = []

    mail_from_address = 'customersuccess@globalknowledge.com'

    # we are only concerned with current students
    all_current_activity = get_all_current_activity(data)

    # report list is all activity for this student
    for enroll_id in enroll_id_arr:
        single_student_activity = all_current_activity[all_current_activity.iloc[:,5] == enroll_id]
        single_student_activity = single_student_activity.set_index(config.SUB_ENROLL_ID)

        # create report in HTML format
        html_report = build_report_html(data, single_student_activity)

        # for emails
        student_info = single_student_activity.iloc[0,:]
        student_name = student_info[config.FIRST_NAME] +' '+ student_info[config.LAST_NAME]
        email = {
            'to': student_info[config.EMAIL],
            'subject': student_name + ' Progress Report',
            'content': html_report
        }
        all_emails.append(email)


    return all_emails

def create_pdf(student, directory):

    style_sheet = getSampleStyleSheet()

    filename = student.name+' Progress Report.pdf'
    report_pdf = SimpleDocTemplate(directory+'/'+filename)
    
    title = student.sub_name + ' Progress Report'
    email = student.complete
    
    content = [
        Image('outputs - pdf/gk-logo-sm.jpg', width=1.136*inch, height=0.5*inch, hAlign='LEFT'),
        Spacer(40,40),
        Paragraph(student.sub_name,style_sheet['Heading1']),
        Paragraph('Progress Report for '+student.name, style_sheet['Heading2']),
        Paragraph(student.email,style_sheet['BodyText']),
        Spacer(20,20),
    ]

    def df_to_2d_arr(df):
        df_data = df.to_numpy()
        data = []
        for row in df_data:
            row_data = []
            for column in row:
                # account for cert links
                if column.startswith('http'):
                    row_data.append(Paragraph('<a href="'+column+'" color="blue"><u>Cert link</u></a>', style_sheet['BodyText']))
                else:
                    row_data.append(Paragraph(column, style_sheet['BodyText']))
            data.append(row_data)
        return data
    
    # Completions
    completions = False
    complete_data = df_to_2d_arr(student.complete)

    # Table(data, colWidths=None, rowHeights=None, style=None, splitByRow=1,repeatRows=0, repeatCols=0, rowSplitRange=None, spaceBefore=None,spaceAfter=None)
    if len(complete_data) > 0:
        completions = True
        content.append(Paragraph('Completions', style_sheet['Heading2']))
        content.append(Table(complete_data, colWidths=[210,80,80,80], hAlign='LEFT'))
    else:
        content.append(Spacer(10,10))
        content.append(Paragraph('No courses completed yet...', style_sheet['BodyText']))
        
    # Progress
    progress_data = df_to_2d_arr(student.progress)

    if len(progress_data) > 0:
        content.append(Paragraph('In Progress', style_sheet['Heading2']))
        content.append(Table(progress_data, colWidths=[210,80,80,80], hAlign='LEFT'))
    elif not completions:
        content.append(Paragraph('And nothing in progress - is everything okay?', style_sheet['BodyText']))
    else:
        content.append(Spacer(10,10))
        content.append(Paragraph('No courses in progress - why not start a new course?', style_sheet['BodyText']))
        
    report_pdf.build(content)
    
    return True
    