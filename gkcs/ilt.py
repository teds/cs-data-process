''' get ILT data '''

import pandas as pd
import numpy as np
from gkcs import bookings
from gkcs import config
from gkcs import subscriptions
from gkcs import enrollments

__ilt = pd.DataFrame()

# for consistency, just returns ilt data
def load():
    return get_ilt()

def set_ilt(ilt_bookings):
    global __ilt

    def add_course_complete(row):
        status = row[config.CRS_STATUS]
        if status == 'Attended':
            return 100
        if status == 'Confirmed':
            return 50
        else: return 0


    ilt = ilt_bookings.copy()

    # Fix sub tags
    ilt[config.SUB_TAG].replace({
        config.ALL_ACCESS['ilt']: config.ALL_ACCESS['name'],
        config.DISCOVERY['ilt']: config.DISCOVERY['name'],
        # config.CISCO_ELITE['ilt']: config.CISCO_ELITE['name']
        }, inplace=True)
    
    # Get correct subscription info and drop 'tag'
    subs_by_tag = subscriptions.get_by_tag()
    ilt = ilt.merge(subs_by_tag, how='left', left_on=config.SUB_TAG, right_index=True)
    ilt.drop(config.SUB_TAG, axis=1, inplace=True)

    # Fix course modality
    ilt[config.CRS_MOD].replace({
        'C-LEARNING': config.ILT_CLASSROOM,
        'V-LEARNING': config.ILT_VIRTUAL
        }, inplace=True)

    # add Completion % based on attended/confirmed
    ilt[config.CRS_COMPLETE] = ilt.apply(add_course_complete, axis=1)

    # add subscription enrollment ID
    ilt = ilt.merge(enrollments.get_by_email_and_code(), how='left', left_on=[config.EMAIL, config.SUB_CODE], right_index=True)

    __ilt = ilt

def get_ilt():
    return __ilt
