''' Accounts '''

from gkcs import config
from gkcs import bookings

__accounts = None

def set_accounts(sub_bookings):
    global __accounts
    __accounts = sub_bookings

# Get dataframe indexed by email
def get_by_email_index():
    accounts_by_email = __accounts[[
        config.EMAIL,
        config.ACCOUNT,
    ]].copy()

    # drop nulls and duplicates
    accounts_by_email = accounts_by_email.drop_duplicates(subset=[config.EMAIL])
    accounts_by_email = accounts_by_email.dropna(subset=[config.EMAIL])
    accounts_by_email = accounts_by_email.set_index(config.EMAIL)

    return accounts_by_email

# Get dataframe indexed by enrollment ID
def get_by_id_index():
    accounts_by_sub_enroll_id = __accounts[[
        config.SUB_ENROLL_ID,
        config.ACCOUNT,
    ]].copy()

    accounts_by_sub_enroll_id = accounts_by_sub_enroll_id.drop_duplicates(subset=[config.SUB_ENROLL_ID])
    accounts_by_sub_enroll_id = accounts_by_sub_enroll_id.dropna(subset=[config.SUB_ENROLL_ID])
    accounts_by_sub_enroll_id = accounts_by_sub_enroll_id.set_index(config.SUB_ENROLL_ID)

    return accounts_by_sub_enroll_id
