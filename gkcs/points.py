''' manage points for subscribers '''

from gkcs import config

# used for progress report only (so far)
def get_weighted_points(df):
    df_points = df.copy()

    # GKLP
    gklp_filter = df_points[config.CRS_MOD] == config.ON_DEMAND_GKLP
    df_points.loc[gklp_filter, config.HEALTH_PTS] = df_points[config.CRS_COMPLETE] * config.WT_GKLP

    # Skillsoft
    ss_filter = df_points[config.CRS_MOD] == config.ON_DEMAND_SKILLSOFT
    df_points.loc[ss_filter, config.HEALTH_PTS] = df_points[config.CRS_COMPLETE] * config.WT_SKILLSOFT

    # All ILT
    ilt_filter = (df_points[config.CRS_MOD] == config.ILT_CLASSROOM) | (df_points[config.CRS_MOD] == config.ILT_VIRTUAL)
    df_points.loc[ilt_filter, config.HEALTH_PTS] = df_points[config.CRS_COMPLETE] * config.WT_ILT

    return df_points
